package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */

    @Test
    public void testEcho()
    {
        App app = new App();
        assertEquals("echoeing its own value is true", 5 , app.echo(5));
    }

    @Test
    public void testOneMore(){
        App app = new App();
        assertEquals("Added one more to its value", 4,app.oneMore(3));
        fail("Did not add to its own value");
    }
}
